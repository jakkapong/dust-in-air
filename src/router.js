import Vue from 'vue'
import Router from 'vue-router'
import Detail from './pages/Detail.vue'
import Home from './pages/Home.vue'

Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/detail',
            name: 'detail',
            component: Detail
        }
    ]
})