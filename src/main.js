import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import Axios from 'axios'
import * as VueGoogleMaps from "vue2-google-maps";

Vue.config.productionTip = false
Axios.defaults.baseURL = 'https://agile-reaches-83574.herokuapp.com/api/'

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyAM1WtMFHFok0gVbp8aa3OsQ1cma-Y6O0c",
    libraries: "places" // necessary for places input
  }
});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
